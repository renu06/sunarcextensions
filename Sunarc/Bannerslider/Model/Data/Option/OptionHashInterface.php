<?php

namespace Sunarc\Bannerslider\Model\Data\Option;

/**
 * Interface OptionHashInterface
 * @package Sunarc\Storelocator\Model\Data\Option
 */
interface OptionHashInterface
{
    /**
     * Return array of options as key-value pairs.
     *
     * @return array Format: array('<key>' => '<value>', '<key>' => '<value>', ...)
     */
    public function toOptionHash();
}
