<?php

namespace Sunarc\Bannerslider\Model;

/**
 * Value Model
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Value extends \Magento\Framework\Model\AbstractModel
{
    /**
     * constructor.
     *
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param \Sunarc\Bannerslider\Model\ResourceModel\Value            $resource
     * @param \Sunarc\Bannerslider\Model\ResourceModel\Value\Collection $resourceCollection
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Sunarc\Bannerslider\Model\ResourceModel\Value $resource,
        \Sunarc\Bannerslider\Model\ResourceModel\Value\Collection $resourceCollection
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    /**
     * load attribute value.
     *
     * @param int    $bannerId
     * @param int    $storeViewId
     * @param string $attributeCode
     *
     * @return $this
     */
//    public function loadAttributeValue($bannerId, $storeViewId, $attributeCode)
//    {
//        $attributeValue = $this->getResourceCollection()
//            ->addFieldToFilter('banner_id', $bannerId)
//            ->addFieldToFilter('store_id', $storeViewId)
//            ->addFieldToFilter('attribute_code', $attributeCode)
//            ->setPageSize(1)->setCurPage(1)
//            ->getFirstItem();
//
//        $this->setData('banner_id', $bannerId)
//            ->setData('store_id', $storeViewId)
//            ->setData('attribute_code', $attributeCode);
//        if ($attributeValue->getId()) {
//            $this->addData($attributeValue->getData())
//                ->setId($attributeValue->getId());
//        }
//
//        return $this;
//    }

    public function loadAttributeValue($bannerId, $storeViewId, $attributeCode)
    {
        $attributeValue = $this->getResourceCollection()
            ->addFieldToFilter('banner_id', $bannerId)
            ->addFieldToFilter('store_id', $storeViewId)
            ->addFieldToFilter('attribute_code', ['in' => $attributeCode]);
//            ->setPageSize(1)->setCurPage(1)
//            ->getFirstItem();
        foreach ($attributeValue as $model) {
            $this->setData('banner_id', $bannerId)
                ->setData('store_id', $storeViewId)
                ->setData('attribute_code', $model->getData('attribute_code'));
            if ($model->getId()) {
                $this->addData($model->getData())
                    ->setId($model->getId());
            }
        }

        return $attributeValue;
    }
}
