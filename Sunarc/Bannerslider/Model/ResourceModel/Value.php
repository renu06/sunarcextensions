<?php

namespace Sunarc\Bannerslider\Model\ResourceModel;

/**
 * Value Resource Model
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Value extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sunarc_bannerslider_value', 'value_id');
    }
}
