<?php

namespace Sunarc\Bannerslider\Model\ResourceModel;

/**
 * Report Resource Model
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Report extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sunarc_bannerslider_report', 'report_id');
    }
}
