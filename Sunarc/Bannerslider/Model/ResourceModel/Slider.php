<?php

namespace Sunarc\Bannerslider\Model\ResourceModel;

/**
 * Slider Resource Model
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Slider extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sunarc_bannerslider_slider', 'slider_id');
    }
}
