<?php

namespace Sunarc\Bannerslider\Model\ResourceModel\Value;

/**
 * Value Collection
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Sunarc\Bannerslider\Model\Value', 'Sunarc\Bannerslider\Model\ResourceModel\Value');
    }
}
