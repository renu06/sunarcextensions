var config = {
    map: {
        '*': {
            'sunarc/note': 'Sunarc_Bannerslider/js/jquery/slider/jquery-ads-note',
            'sunarc/impress': 'Sunarc_Bannerslider/js/report/impress',
            'sunarc/clickbanner': 'Sunarc_Bannerslider/js/report/clickbanner',
        },
    },
    paths: {
        'sunarc/flexslider': 'Sunarc_Bannerslider/js/jquery/slider/jquery-flexslider-min',
        'sunarc/evolutionslider': 'Sunarc_Bannerslider/js/jquery/slider/jquery-slider-min',
        'sunarc/popup': 'Sunarc_Bannerslider/js/jquery.bpopup.min',
    },
    shim: {
        'sunarc/flexslider': {
            deps: ['jquery']
        },
        'sunarc/evolutionslider': {
            deps: ['jquery']
        },
        'sunarc/zebra-tooltips': {
            deps: ['jquery']
        },
    }
};
