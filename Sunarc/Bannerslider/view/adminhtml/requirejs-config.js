var config = {
    map: {
        '*': {
            'sunarc/note': 'Sunarc_Bannerslider/js/jquery/slider/jquery-ads-note',
        },
    },
    paths: {
        'sunarc/flexslider': 'Sunarc_Bannerslider/js/jquery/slider/jquery-flexslider-min',
        'sunarc/evolutionslider': 'Sunarc_Bannerslider/js/jquery/slider/jquery-slider-min',
        'sunarc/zebra-tooltips': 'Sunarc_Bannerslider/js/jquery/ui/zebra-tooltips',
    },
    shim: {
        'sunarc/flexslider': {
            deps: ['jquery']
        },
        'sunarc/evolutionslider': {
            deps: ['jquery']
        },
        'sunarc/zebra-tooltips': {
            deps: ['jquery']
        },
    }
};
