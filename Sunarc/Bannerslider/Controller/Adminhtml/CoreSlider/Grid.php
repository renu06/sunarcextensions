<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\CoreSlider;

/**
 * Core slider Grid action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Grid extends \Sunarc\Bannerslider\Controller\Adminhtml\CoreSlider
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();

        return $resultLayout;
    }
}
