<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml;

/**
 * Slider Abstract Action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
abstract class Slider extends \Sunarc\Bannerslider\Controller\Adminhtml\AbstractAction
{
    const PARAM_CRUD_ID = 'slider_id';

    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Sunarc_Bannerslider::bannerslider_sliders');
    }
}
