<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml;

/**
 * Report Abstract Action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
abstract class Report extends \Sunarc\Bannerslider\Controller\Adminhtml\AbstractAction
{
    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Sunarc_Bannerslider::bannerslider_reports');
    }
}
