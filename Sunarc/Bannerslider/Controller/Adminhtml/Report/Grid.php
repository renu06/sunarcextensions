<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Report;

/**
 * Report grid action.
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Grid extends \Sunarc\Bannerslider\Controller\Adminhtml\Report
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();

        return $resultLayout;
    }
}
