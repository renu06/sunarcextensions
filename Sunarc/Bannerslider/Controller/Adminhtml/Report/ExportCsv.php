<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Report;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * ExportCsv action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class ExportCsv extends \Sunarc\Bannerslider\Controller\Adminhtml\Report
{
    public function execute()
    {
        $fileName = 'reports.csv';

        /** @var \\Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $content = $resultPage->getLayout()->createBlock('Sunarc\Bannerslider\Block\Adminhtml\Report\Grid')->getCsv();

        return $this->_fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
    }
}
