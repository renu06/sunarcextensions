<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Banner;

/**
 * NewAction
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class NewAction extends \Sunarc\Bannerslider\Controller\Adminhtml\Banner
{
    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
