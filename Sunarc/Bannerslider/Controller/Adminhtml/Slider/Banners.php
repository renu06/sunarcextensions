<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Slider;

/**
 * Banners of slider action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Banners extends \Sunarc\Bannerslider\Controller\Adminhtml\Slider
{
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('bannerslider.slider.edit.tab.banners')
                     ->setInBanner($this->getRequest()->getPost('banner', null));

        return $resultLayout;
    }
}
