<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Slider;

/**
 * Preview Slider action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Preview extends \Sunarc\Bannerslider\Controller\Adminhtml\Slider
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;
    }
}
