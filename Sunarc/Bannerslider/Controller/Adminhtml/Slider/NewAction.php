<?php
namespace Sunarc\Bannerslider\Controller\Adminhtml\Slider;

/**
 * New Slider Action
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class NewAction extends \Sunarc\Bannerslider\Controller\Adminhtml\Slider
{
    public function execute()
    {
        $resultForward = $this->_resultForwardFactory->create();

        return $resultForward->forward('edit');
    }
}
