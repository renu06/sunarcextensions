<?php

namespace Sunarc\Bannerslider\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install schema
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

            $setup->startSetup();

        if (version_compare($context->getVersion(), '1.7.2') < 0) {
            // Get module table

            $tableName = $setup->getTable('sunarc_bannerslider_banner');

            // Check if the table already exists

            if ($setup->getConnection()->isTableExists($tableName) == true) {
            // Declare data

                $columns = [

                'video' => [

                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

                'nullable' => true,

                'afters' => 'image_alt',

                'comment' => 'Banner video',

                ],
                'video_thumb' => [

                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

                'nullable' => true,

                'afters' => 'video',

                'comment' => 'Banner video thumb',

                ],

                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }
            }

            
            if (version_compare($context->getVersion(), '1.7.3') < 0) {

            // Get module table

            $tableName = $setup->getTable('magestore_bannerslider_banner');

            // Check if the table already exists

            if ($setup->getConnection()->isTableExists($tableName) == true) {

            // Declare data

            $columns = [

            'type' => [

            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

            'nullable' => true,

            'afters' => 'image_alt',

            'comment' => 'Banner Type',

            ],

            ];

            $connection = $setup->getConnection();

            foreach ($columns as $name => $definition) {

                $connection->addColumn($tableName, $name, $definition);

                }

     
            }

        }


        }

 
        $setup->endSetup();
    }
}
