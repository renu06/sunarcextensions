<?php
namespace Sunarc\Bannerslider\Block\Adminhtml;

/**
 * Report grid container.
 * @category Sunarc
 * @package  Sunarc_Bannerslider
 * @module   Bannerslider
 * @author   Sunarc Developer
 */
class Report extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_report';
        $this->_blockGroup = 'Sunarc_Bannerslider';
        $this->_headerText = __('Reports');
        parent::_construct();
        $this->removeButton('add');
    }
}
