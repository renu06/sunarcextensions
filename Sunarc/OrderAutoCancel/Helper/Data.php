<?php

namespace Sunarc\OrderAutoCancel\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    protected $_directoryList;

    public function __construct(

        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    )
    {
        parent::__construct($context);
        $this->_directoryList = $directoryList;
    }


    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function printLog($data, $flag = 1, $filename = "orderautocancel.log")
    {
        if ($flag == 1) {
            $path = $this->_directoryList->getPath("var");
            $logger = new \Zend\Log\Logger();
            if (!file_exists($path . "/log/"))
                mkdir($path . "/log/", 0777, true);
            $logger->addWriter(new \Zend\Log\Writer\Stream($path . "/log/" . $filename));
            if (is_array($data) || is_object($data))
                $data = print_r($data, true);
            $logger->info($data);
        }
    }


}
