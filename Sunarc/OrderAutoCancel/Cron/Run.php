<?php

namespace Sunarc\OrderAutoCancel\Cron;
use \Psr\Log\LoggerInterface;
use Magento\Sales\Controller\OrderInterface;


class Run
{
    protected $_objectManager;
    protected $_order;
    protected $_helper;
    protected $_orderCollectionFactory;

    public function __construct(\Magento\Framework\App\ObjectManager $objectManager,
                                \Magento\Sales\Api\OrderManagementInterface $orderManagementInterface,
                                \Sunarc\OrderAutoCancel\Helper\Data $helper,
                                \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
                                \Magento\Customer\Model\Customer $customer)
    {

        $this->_objectManager = $objectManager;
        $this->_order = $orderManagementInterface;
        $this->_helper = $helper;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_customer = $customer;
    }

    public function execute()
    {
        $moduleStatus = $this->_helper->getConfig('orderautocancel/general/enable');
        $considerAfter = $this->_helper->getConfig('orderautocancel/general/cosider_after');
        $duration = $this->_helper->getConfig('orderautocancel/general/duration');
        $unit = $this->_helper->getConfig('orderautocancel/general/unit');


        if ($moduleStatus) {


            $fromDate = date('Y-m-d H:i:s', strtotime($considerAfter));
            $now = date('Y-m-d H:i:s');
            // Get Pending Order List

            $orderIds = [];
            $orders = $this->_orderCollectionFactory->create()
                ->addFieldToSelect('*')
                // ->addFieldToFilter('created_at', ['lteq' => $toDate])
                ->addFieldToFilter('created_at', ['gteq' => $fromDate])
                ->addFieldToFilter('status', ['in' => 'pending'])->setOrder(
                    'created_at',
                    'desc'
                );
           // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();


            foreach ($orders as $order) {
                $orderId = $orderIds[] = $order->getEntityId();
                $createdTime = $order->getCreatedAt();
                $endTime = date('Y-m-d H:i:s', strtotime('+' . $duration . ' ' . $unit, strtotime($createdTime)));
                if ($endTime <= $now) {
                   $order = $this->_objectManager->create('\Magento\Sales\Model\Order')
                        ->load($orderId);
                   //$status =  $this->_order->cancel($orderId);
                  //  $this->_helper->printLog('Order Cancel: ' . $status, 1);
                   $order->setState("closed")->setStatus("closed");

                    $order->save();
                    $this->_helper->printLog('Order Closed ID: ' . $orderId, 1);
                }

            }

            $this->_helper->printLog('Order Checking: ', 1);


        }
    }
}

