<?php
namespace Sunarc\OrderAutoCancel\Model\Config\Source;

class TimeUnit implements \Magento\Framework\Option\ArrayInterface
{
public function toOptionArray()
{
return [
['value' => 'minutes', 'label' => __('Minutes')],
['value' => 'hour', 'label' => __('Hours')],
['value' => 'day', 'label' => __('Days')]
];
}
}